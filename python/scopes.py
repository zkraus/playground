

# function scope
def func():
    x = 1

print 'function scope'
func()
try:
    print 'var x: %s' % x
    del x
except NameError as exc:
    print 'var x: %s' % exc

print

# outer/inner function

print 'outer/inner function scope'
def out_func():
    print "enter out_func()"
    outer = 1
    def inn_func():
        print "  enter inn_func()"
        inner = 2

        try:
            print '  .var outer: %s' % outer
        except NameError as exc:
            print '  .var outer: %s' % exc
        print "  leave inn_func()"
    inn_func()

    try:
        print '.var inner: %s' % inner
    except NameError as exc:
        print '.var inner: %s' % exc

    try:
        print '.var outer: %s' % outer
    except NameError as exc:
        print '.var outer: %s' % exc

    print "leave out_func()"

out_func()

print

# inner function call
try:
    inn_func()
except NameError as exc:
    print 'call inn_func(): %s' % exc

print

# if scope
print "if scope"
if True:
    x = 1

try:
    print 'var x: %s' % x
    del x
except NameError as exc:
    print 'var x: %s' % exc

print

# for scope
print "for scope"

for x in range(10):
    y = 2*x

try:
    print 'var x: %s' % x
    del x
except NameError as exc:
    print 'var x: %s' % exc

try:
    print 'var y: %s' % y
    del y
except NameError as exc:
    print 'var y: %s' % exc

print

# false for scope
print "false for scope"
for x in []:
    y = 1

try:
    print 'var x: %s' % x
    del x
except NameError as exc:
    print 'var x: %s' % exc

try:
    print 'var y: %s' % y
    del y
except NameError as exc:
    print 'var y: %s' % exc

print

# lambda scope
print "lambda external scope"
ll = lambda: a

try:
    print "calling ll()"
    print ll()
except NameError as exc:
    print exc

print
print 'setting outer value a'
a = 1

try:
    print "calling ll()"
    print ll()
except NameError as exc:
    print exc

del a


# enclosure inner lambda scope
print "enclosure inner lambda scope"
a = 4

def enc_func(xx):
    try:
        print "calling xx()"
        print xx()
    except NameError as exc:
        print "xx(): %s" % exc

enc_func(ll)
del a

print
