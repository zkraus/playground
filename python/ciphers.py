
ord_min = ord('a')
ord_max = ord('z')
ord_mod = ord_max - ord_min


def Lowercase(funct):
    def lowerizer(in_text, passphrase, x_funct):
        return funct(in_text.lower(), passphrase.lower(), x_funct)
    return lowerizer

def numerize(char):
    return ord(char) - ord_min


def characterize(num_char):
    return chr(num_char + ord_min)


@Lowercase
def _caesar_base(in_text, passphrase, transform_funct):
    offset = numerize(passphrase)
    out_text = ""
    for in_char in in_text:
        if in_char in [' ']:
            out_text += in_char
        else:
            out_text += characterize( transform_funct(numerize(in_char), offset) % ord_mod)
    return out_text


def caesar_encrypt(plain_text, passphrase):
    return _caesar_base(plain_text, passphrase, lambda x,y: x+y)


def caesar_decrypt(cipher_text, passphrase):
    return _caesar_base(cipher_text, passphrase, lambda x,y: x-y)


@Lowercase
def _vigener_base(in_text, passphrase, transform_funct):
    offset_list = [numerize(p_char) for p_char in passphrase]
    out_text = ''

    passphrase_index = 0
    passphrase_length = len(passphrase)

    for in_char in in_text:
        if in_char in [' ']:
            out_text += in_char
        else:
            out_text += characterize(transform_funct(numerize(in_char),offset_list[passphrase_index]) % ord_mod)
            passphrase_index += 1
            if passphrase_index >= passphrase_length:
                passphrase_index = 0
    return out_text


def vigener_encrypt(plain_text, passphrase):
    return _vigener_base(plain_text, passphrase, lambda x,y: x+y)


def vigener_decrypt(cipher_text, passphrase):
    return _vigener_base(cipher_text, passphrase, lambda x,y: x-y)


if __name__ == '__main__':
    plain_text = "hello world"

    passphrase = 'c'

    print 'Plain text: %s' % (plain_text)

    print "--- Caesar Cipher ---"
    print 'Caesar passphrase: %s' % (passphrase)

    cipher_text = caesar_encrypt(plain_text, passphrase)
    print 'Caesar ciphertext: %s' % (cipher_text)
    new_plain_text = caesar_decrypt(cipher_text, passphrase)
    print 'Caesar plaintext: %s' % (new_plain_text)

    print
    print "--- Vigener Cipher ---"
    passphrase_vig = 'key'
    print 'Vigener passphrase: %s' % (passphrase_vig)

    vig_cipher_text = vigener_encrypt(plain_text, passphrase_vig)
    print 'Vigener ciphertext: %s' % (vig_cipher_text)
    vig_plain_text = vigener_decrypt(vig_cipher_text, passphrase_vig)
    print 'Vigener plaintext: %s' % (vig_plain_text)

    print '--- END ---'
