import copy

class prefixt(dict):
    def add(self, item, pos):
        if item in self:
            self[item][0] += 1
            self[item][1].append(pos)
        else:
            self[item] = [1, [pos]]

    def filter(self, upto=1):
        for k, v in copy.copy(self).iteritems():
            if v[0] <= upto:
                del self[k]

pdict = prefixt()

def despace(data):
    return data.replace(' ', '')

def scan(data, min_len=2, max_len=4):
    start = 0
    stop = min_len
    data_len = len(data)
    prefix_dict = prefixt()
    if max_len > data_len:
        max_len = data_len
    while start+max_len <= data_len:
        stop = min_len
        while stop <= max_len:
            prefix_dict.add(data[start:start+stop], start)
            stop += 1
        start += 1
    return prefix_dict



string = "hello the world i am the program"
print despace(string)
prefix_dict = scan(despace(string))
prefix_dict.filter()
print prefix_dict



