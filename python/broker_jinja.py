from __future__ import print_function

from collections import namedtuple

from jinja2 import Environment, PackageLoader

env = Environment(
    loader=PackageLoader('python', 'jinja_templates'),
    trim_blocks=True,
    lstrip_blocks=True,
)

broker_template = env.get_template('broker.xml_jinja')

Acceptor = namedtuple('Acceptor', ['name', 'uri'])

SecuritySetting = namedtuple('SecuritySetting', ['match', 'rules'])

SecurityRule = namedtuple('SecurityRule', ['roles', 'type'])

BrokerModel = namedtuple('BrokerModel', ['acceptors', 'addresses', 'security_settings'])

Address = namedtuple('Address', ['name', 'anycast', 'multicast'])

Queue = namedtuple('Queue', ['name'])

my_broker = BrokerModel(
    acceptors=[
        Acceptor(name='amqp', uri='amqp://localhost:5672'),
        Acceptor(name='main', uri='tcp://localhost:61616'),
    ],
    addresses=[
        Address(
            name='example',
            anycast=[
                Queue('example'),
                Queue('example_backup'),
            ],
            multicast=None
        ),
        Address(
            name='news',
            anycast=[
                Queue('newsBackup'),
            ],
            multicast=[
                Queue('Europe'),
                Queue('America'),
                Queue('Asia')
            ],
        )
    ],
    security_settings=[
        SecuritySetting(
            match='#',
            rules=[
                SecurityRule('guest', 'createDurableQueue'),
                SecurityRule('guest', 'deleteDurableQueue'),
                SecurityRule('guest', 'send'),
                SecurityRule('guest', 'consume'),
            ]
        )
    ]
)

broker_config = broker_template.render(broker=my_broker)

print(broker_config)
