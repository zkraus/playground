import itertools
import operator

a = [0, 1, 2, 3, 4]
b = ['a', 'b', 'c', 'd', 'e']


def section(name):
    print('\n# {} ---'.format(name))


section('accumulate (prefix sum)')
print(list(itertools.accumulate(a, operator.add)))

section('chain iterators')
print(list(itertools.chain(range(3), range(3))))

section('combinations')
print(list(itertools.combinations(a, 2)))

section('combinations_with_replacement')
print(list(itertools.combinations_with_replacement(a, 2)))

section('permutations')
print(list(itertools.permutations(a, 2)))

section('compress')
# binary filter
print(list(itertools.compress(a, [0, 1, 1, 0, 1])))

section('dropwhile')
print(list(itertools.dropwhile(lambda x: x <= 3, a)))

section('takewhile')
print(list(itertools.takewhile(lambda x: x <= 3, a)))


section('count')
# run to infinity
for i in itertools.count():
    print(i)
    if i > 4:
        break

section('islice')
# iterator slice
print(list(itertools.islice(itertools.count(), 2, 7)))
