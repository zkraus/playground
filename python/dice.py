
import random
import re

class Die:
  """Dice implementation"""
  def __init__(self, base=6, begin=1):
    self.modbase = base - 1
    self.base = self.modbase + begin
    self.begin = begin

  def __str__(self):
    return "%d sided die" % self.base

  def roll(self, times=1):
    if times == 1:
      return random.randint(self.begin,self.base)
    else:
      return [random.randint(self.begin,self.base) for x in range(times)]

class Sack:
  """Dice sack, simplifies many dice rolling"""
  def __init__(self, dice=[]):
    self.dice = dice

  def __str__(self):
    return "Sack with %s" % [x.base for x in self.dice]

  def roll(self, times=1):
    return [x.roll() for i in range(times) for x in self.dice]

  def sum_roll(self, times=1):
    tmp = self.roll(times)
    return reduce(lambda x,y: x + y, tmp)

  def add_die(self, die):
    self.dice.append(die)

  def remove_die(self, pos=None):
    if not pos:
      pos = -1
    self.dice.pop(pos)

  def clear_sack(self):
    self.dice = []

## DEFAULT DICE VALUES
d4 = Die(4)
d6 = Die(6)
d8 = Die(8)
d10 = Die(10)
d010 = Die(10,0)
d12 = Die(12)
d16 = Die(16)
d20 = Die(20)
complete_sack = Sack([d4, d6, d8, d10, d12, d16, d20])
##


## CHARACTER STATS VALUES
stat_values = { 1:-5, 2:-4, 3:-4, 4:-3, 5:-3, 6:-2, 7:-2, 8:-1, 9:-1, 
                10:0, 11:0, 12:0, 
                13:+1, 14:+1, 15:+2, 16:+2, 17:+3, 18:+3, 19:+4, 20:+4, 21:+5 }

class DiceRange:
  """Dice range abstraction"""
  def __init__(self, value, name="", comment=""):
    self.origin = value
    self.name = name
    self.comment = comment
    self.parse(value)

  def __str__(self):
    return "DiceRange %s (%s, %s) originated by %s used for %s" \
            % (self.name,\
               self.text_range,\
               self.dice_range,\
               self.origin,\
               self.comment)

  def parse(self, value):
    if '-' in str(value):
      tmp = value.rsplit('-')
      if len(tmp) != 2:
        raise ValueError("DiceRange.parse() bad input text range supplied '%s'"\
                         % value)
      self.parserange(tmp[0], tmp[1])
    else:
      self.parsespec(value)

  def parsespec(self, value):
    if '+' in value:
      dice, self.static = value.split('+')
      self.static = int(self.static)
    else:
      dice = value
      self.static = 0

    self.diecount, self.diebase = [int(x) for x in re.split('[dkDK]', dice)] # [dkDK] must be supplied, re.I doesn't work
    if not self.diecount:
      self.diecount = 1

    self.low = self.static + self.diecount
    self.high = self.static + self.diecount * self.diebase

    self.basedie = None
    for d in complete_sack.dice:
      if d.base == self.diebase:
        self.basedie = d
        break

    if not self.basedie:
      self.basedie = Die(self.diebase)

    self.commit()

  def parserange(self, low, high):
    self.low = int(low)
    self.high = int(high)
    diebase = self.high - self.low
    #print "Diebase = %d" % diebase
    if diebase < 0:
      raise ValueError("DiceRange.parse() bad input text range supplied '%s'"\
                        % value)
    ## go through complete sack and check modulo
    modresult = [diebase % x.modbase for x in complete_sack.dice]
    #print "Modresult %s" % modresult
    ## find largest index base normalized for complete_sack
    tmpmodresult = modresult
    tmpmodresult.reverse()
    #print "tmpModresult %s" % tmpmodresult
    modindex = len(complete_sack.dice) - tmpmodresult.index(0) - 1
    #print "modindex = %d" % modindex

    self.basedie = complete_sack.dice[modindex]
    self.diecount = diebase / self.basedie.modbase
    #print "diecount = %d" % self.diecount

    ## add begin as static die
    self.static = self.low - self.diecount

    ## NEEDED IN ALL CASES
    ## commit parsed values to text representations and make_sack
    self.commit()

  def make_sack(self):
    self.sack = Sack([self.basedie for i in range(self.diecount)])
    self.create_static_die()

  ## set captions ranges etc. from parsed values
  def commit(self):
    self.text_range = "%d-%d" % (self.low, self.high)
    self.dice_range = "%dk%d+%d" % (self.diecount, self.basedie.base, \
                                    self.static)
    self.make_sack()

  def roll(self):
    return self.sack.sum_roll()

  def apply_offset(self, offset, name=None, comment=None):
    if name:
      self.name = name
    if comment:
      self.comment = comment
    self.origin += "+of(%d)" % offset
    self.sack.remove_die() ## static die
    self.static += offset
    self.low += offset
    self.high += offset
    self.commit()

  def create_static_die(self):
    if self.diecount == len(self.sack.dice):
      self.sack.add_die(Die(1,self.static))


