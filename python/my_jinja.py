from __future__ import print_function

from jinja2 import Template, Environment, PackageLoader

t = Template('Hello {{ name }}')
print(t.render(name='John Doe'))

t = Template('Numbers: {% for i in range(10) %}{{ i**2 }} {% endfor %}.')
print(t.render())

env = Environment(
    loader=PackageLoader('python', 'jinja_templates')
)

t = env.get_template('list_data.jinja')
print(t.render(name='Test', items=['asdf', 'qwerty', 'uiop']))
