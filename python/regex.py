#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re


data = [
  u'Košinova 18, 612 00 Brno-Královo Pole, Czechia',
  u'Košinova, 612 00 Brno-Brno-Královo Pole, Czechia',
  u'Revúcká 1227/12, 784 01 Litovel, Czechia',
  u'Neuer Steinweg 26, 20459 Hamburg, Germany',
  u'World Way, Los Angeles, CA 90045, USA',
  u'Simmeringer Hauptstraße 234, 1110 Wien, Austria',
  u'Purkyňova 1764/12, 612 00 Brno-Brno-Královo Pole, Czechia',
  u'Brno, Czechia',
  u'Czechia',
  u'Norberčany 18, 793 05 Norberčany, Czechia',
  u'Stará Libavá, 793 05 Norberčany, Czechia',
]

wild = '.*'
wild_space = '\s*'
space = '\s+'
separator = ',?\s*'

street_name = '(.*)'
street_global_number= '(([0-9]+)/)?'
street_local_number = '([0-9]+[a-zA-Z]*)'
zip_code = '([0-9]+\s*[0-9]+)'
city = '(.*)'
state = '(.*)'



addr_list = [
  wild_space,
  street_name,
  wild_space,
  street_global_number,
  street_local_number,
  separator,
  zip_code,
  wild_space,
  city,
  separator,
  state,
  wild,
]

full_addr = "".join(addr_list)
print full_addr
rex = re.compile(full_addr)

for i in data:
  print i
  x = rex.search(i)
  if x:
    print x.groups()
  print

