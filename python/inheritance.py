class Base(object):

    def __init__(self):
        self.value = None


class Inter(Base):
    aux = 1

    def __init__(self):
        super(Inter, self).__init__()
        self.inst_aux = 2


class Leaf(Inter):
    value = 1


b = Base()
l = Leaf()
m = Leaf()

# print b.value
print l.value
print m.value

print "-----"
l.value = 3

# print b.value
print l.value
print m.value


