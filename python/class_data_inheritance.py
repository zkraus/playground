#!/usr/bin/env python2

class A(object):
    default_data = {
        'a': 1,
        'aa': 1,
    }

    def __init__(self):
        print "init a via (%s)" % self.__class__.__name__
        self.data = {}
        self._refresh_default_data()
        self.data.update(self.default_data)

    def _refresh_default_data(self):
        for klass in reversed(type.mro(self.__class__)):
            if hasattr(klass, 'default_data'):
                self.data.update(getattr(klass, 'default_data'))


class B(A):
    default_data = {
        'aa': 3,
        'b': 2
    }


class C(B):
    default_data = {
        'c': 3
    }


class D(C):
    default_data = {
        'd': 4
    }

x = [
    ('a', A()),
    ('b', B()),
    ('c', C()),
    ('d', D()),
]

for k, v in x:
    print "%s = %s" % (k, v.data)
