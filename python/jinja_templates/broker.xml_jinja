{#  Jinja 2 template for Artemis broker.xml #}
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<configuration xmlns="urn:activemq" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:schemaLocation="urn:activemq /schema/artemis-configuration.xsd">
    <core xmlns="urn:activemq:core">

        <persistence-enabled>false</persistence-enabled>

        <acceptors>
        {% for acceptor in broker.acceptors %}
            <acceptor name={{ acceptor.name }}>{{ acceptor.uri }}</acceptor>
        {% endfor %}
        </acceptors>

        <!-- Other config -->

        <security-settings>
            <!--security for example queue-->
        {% for security in broker.security_settings %}
            <security-setting match="{{ security.match }}">
            {% for rule in security.rules %}
                <permission roles="{{ rule.roles }}" type="{{ rule.type }}"/>
            {% endfor %}
            </security-setting>
        {% endfor %}
        </security-settings>

        <addresses>
        {% for address in broker.addresses %}
            <address name="{{ address.name }}">
            {% if address.anycast %}
                <anycast>
                {% for queue in address.anycast %}
                    <queue name="{{ queue.name }}"/>
                {% endfor %}
                </anycast>
            {% endif %}
            {% if address.multicast %}
                <multicast>
                {% for queue in address.multicast %}
                    <queue name="{{ queue.name }}"/>
                {% endfor %}
                </multicast>
            {% endif %}
            </address>
        {% endfor %}
        </addresses>
    </core>
</configuration>