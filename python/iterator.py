class IIter(object):
  def __init__(self, a_list, b_list):
    self.a_list = a_list
    self.b_list = b_list

  def iter_a(self):
    for a in self.a_list:
      data = {'a': a}
      yield data

  def iter_b(self):
    for b in self.b_list:
      data = {'b': b}
      yield data

  def iter_ab(self):
    for a in self.iter_a():
      for b in self.iter_b():
        data = {}
        data.update(a)
        data.update(b)
        yield data



iii = IIter([1, 2, 3], ['x', 'y', 'z'])

for i in iii.iter_ab():
  print i



def empty_iter():
  raise StopIteration
  yield None

for i in empty_iter():
  print i
