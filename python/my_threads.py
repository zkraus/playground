import threading
import time
import random


class MyThread(threading.Thread):
    def __init__(self, data):
        super(MyThread, self).__init__()
        self.data = data

    def run(self):
        i = 0
        while i < 10:
            tmp = "%s%d" % (self.data, i)
            lock1.acquire()
            data1.append(tmp)
            # print(tmp)
            lock1.release()
            time.sleep(0.0001 * random.randint(1, 256))
            i += 1


lock1 = threading.Lock()
data1 = []

thr_pool = [MyThread(x) for x in 'abcdefg']

# map(threading.Thread.start, thr_pool)


for t in thr_pool:
    t.start()

for t in thr_pool:
    t.join()

print(data1)
print()
print("Exit Main Thread")
