#include <iostream>
#include <exception>
#include <stdexcept>


class Node {
public:
    int data;
    Node* next;

    Node(int data): data(data), next(NULL) { }
    ~Node();
    void del();
};

Node::~Node()
{
    if (this->next) {
        delete this->next;
    }
}


class List {
    Node* node;
    unsigned int length;
public:
    List();
    ~List();
    void insert(Node& item);
    Node& get_item(unsigned int k);
    int& get_data(unsigned int k);
    unsigned int len();

    void reverse();
};

List::List(): node(NULL), length(0) { }
List::~List() {
    if (this->node) {
        delete this->node;
    }
}

void
List::insert(Node &item)
{
    Node* tmp = this->node; // get the previous first
    Node* curr = &item;
    this->node = curr; // inserted as first in list
    this->length++; // adding at least one

    // if inserted node has other linked items
    while (curr->next) {
        this->length++; // add length
        curr = curr->next; // move to next
    }

    curr->next = tmp; // join new.last with old.first
}

Node&
List::get_item(unsigned int k)
{
    if (k > this->length) {
        throw std::out_of_range("End of List");
    }
    Node* curr = this->node;
    unsigned int i = 0;
    while (i<k) {
        curr = curr->next;
        i++;
    }
    return *curr;
}

int&
List::get_data(unsigned int k)
{
    return this->get_item(k).data;
}

unsigned int
List::len() {
    return this->length;
}

void
List::reverse()
{
    Node* rev = NULL;
    Node* curr = this->node;
    Node* head = NULL;

    while (curr) {
        head = curr; // save head
        curr = head->next; // move current
        head->next = rev; // link rev list
        rev = head; // set as rev head
    }
    this->node = rev; // set rev as the list
}

std::ostream& operator<<(std::ostream& os, List& list)
{

    os << "List[";
    unsigned int i = 0;
    try{
        while (i < list.len()) {
            os << list.get_data(i);
            i++;
        }
    } catch (std::exception& e) {

    }
    os << "]";
    return os;
}



List& spawn_list(unsigned int k)
{
    List* list = new List();
    //Node* node = NULL;
    for (unsigned int i = k; i-- > 0;) {
        list->insert(*new Node(i));
    }
    return *list;
}


Node& one()
{
    Node* tmp = new Node(1);
    return *tmp;
}

int main()
{
    List list = spawn_list(10);

    //std::cout << &list.get_item(0) << std::endl;
    std::cout << list << std::endl;
    list.reverse();

    std::cout << list << std::endl;


    //delete &list;

    return 0;
}
