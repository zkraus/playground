#include "TcpSocket.h"

std::string TcpSocket::state_text[3] = {"Closed", "Open", "Listen"};

TcpSocket::TcpSocket(std::string _host, unsigned int _port)
{
  this->host = _host;
  this->port = _port;
  this->state = this->CLOSED;
}

TcpSocket::~TcpSocket()
{
  this->close();
  std::cout << "see you" << std::endl;
}

void TcpSocket::close()
{
  if (this->state != this->CLOSED)
  {
    // TODO close socket
    this->state = this->CLOSED;
  }
}


void TcpSocket::connect()
{
  if (this->state == this->CLOSED)
  {
    //open
    this->state = this->OPEN;
  } else {
    // throw already opened
  }
}

void TcpSocket::listen()
{
  if (this->state == this->CLOSED)
  {
    // todo open for listen
    this->state = this->LISTEN;
  }
}
