#ifndef __TCP_SOCKET_H__
#define __TCP_SOCKET_H__

#include<string>
#include<iostream>
#include<sys/types.h>
#include<sys/socket.h>


class TcpSocket
{
    enum e_state {CLOSED = 0, OPEN = 1, LISTEN = 2};
    static std::string state_text[3];
    std::string host;
    unsigned int port;
    e_state state;
  public:
    TcpSocket(std::string, unsigned int);
    ~TcpSocket();

    void close();
    void connect();
    void listen();

    friend std::ostream& operator<<(std::ostream& os, const TcpSocket& _socket)
    {
      return os << "TcpSocket(" << _socket.host << ":" << _socket.port << "," << _socket.state_text[_socket.state] << ")";
    }


};


#endif
