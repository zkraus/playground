#include<iostream>
#include<string>

#include "TcpSocket.h"

int main(int argc, char ** argv)
{
  TcpSocket tcp_socket = TcpSocket("127.0.0.1", 12000);

  std::cout << tcp_socket << std::endl;

  tcp_socket.connect();
  std::cout << tcp_socket << std::endl;

  tcp_socket.close();
  std::cout << tcp_socket << std::endl;

  tcp_socket.listen();
  std::cout << tcp_socket << std::endl;
  return 0;
}
