#ifndef __MYSQRT_H__
#define __MYSQRT_H__

#include <math.h>
#include <unistd.h>

double mysqrt(double input) {
  if (input < 0) {
    return 0;
  }
  return sqrt(input);
}

#endif
