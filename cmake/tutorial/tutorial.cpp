// simple tutoria program
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "TutorialConfig.h"

#ifdef USE_MATH_FUNCTIONS
#include "MathFunctions.h"
#endif

int main(int argc, char ** argv) {

  if (argc < 2) {
    fprintf(stderr, "%s Version %d.%d\n", argv[0], Tutorial_VERSION_MAJOR, Tutorial_VERSION_MINOR);
    fprintf(stderr, "Usage: %s <number>\n", argv[0]);
    return 2;
  }

  double input = atof(argv[1]);
#ifdef USE_MATH_FUNCTIONS
  double output = mysqrt(input);
#else
  double output = sqrt(input);
#endif

  fprintf(stdout, "Result: %g\n", output);

  return 0;
}
