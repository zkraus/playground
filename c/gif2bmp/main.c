
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <unistd.h>
#include <getopt.h>

#include "gif2bmp.h"

#define OPT_IN   0x01
#define OPT_OUT  0x02
#define OPT_LOG  0x04
#define OPT_IMGS 0x03
#define OPT_HELP 0x08


int main (int argc, char ** argv) {

  int optFlag = 0;

  char * inFileName = NULL;
  char * outFileName = NULL;
  char * logFileName = NULL;

  int opt;

  FILE * in = NULL;
  FILE * out = NULL;
  FILE * log = NULL;

  tGIF2BMP result;

  while ((opt = getopt(argc, argv, "i:o:l:h")) != -1) {
    switch (opt) {
      case 'i':
        optFlag |= OPT_IN;
        inFileName = optarg;
        break;
      case 'o':
        optFlag |= OPT_OUT;
        outFileName = optarg;
        break;
      case 'l':
        optFlag |= OPT_LOG;
        logFileName = optarg;
        break;
      case 'h':
        optFlag |= OPT_HELP;
        break;
      case '?':
        fprintf(stderr, "Bad parameters\n");
        return 1;
        break;

      default:
        fprintf(stderr, "Getopt Failure\n");
        return 1;
        break;
    }

  }

  if ((optFlag & OPT_HELP) != 0) {
    printf("gif2bmp {-i <gifImage> -o <bmpImage> [-l <logFile>] | -h}\n");
    printf("  -i <gifImage>   gives input GIF file to convert\n");
    printf("  -o <bmpImage>   gives output filename to save converted BMP\n");
    printf("  -l <logFile>    optional Logging file to write statisctics\n");
    printf("  -h              Prints this help\n");
    return 0;
  }

  if ((optFlag & (OPT_IMGS)) != OPT_IMGS) {
    fprintf(stderr, "Insufficient filenames given\n");
    return 1;
  }


    if ((in = fopen(inFileName, "rb")) == NULL) {
      fprintf(stderr, "Could not open a file %s", inFileName);
      return EFILE;
    }

    if ((out = fopen(outFileName, "wb")) == NULL) {
      fprintf(stderr, "Could not open a file %s", outFileName);
      return EFILE;
    }

    if (gif2bmp(&result, in, out) != 0) {
      fclose(in);
      fclose(out);
      return 1;
    }


  if ((optFlag & OPT_LOG) != 0) {
    if ((log = fopen(logFileName, "w")) == NULL) {
      fprintf(stderr, "Could not open a file %s", logFileName);
      return EFILE;
    }

    fprintf(log, "login = xkraus00\nuncodedSize = %lld\ncodedSize = %lld\n",
            result.gifSize, result.bmpSize);

   

   fclose(log);
  }


    fclose(in);
    fclose(out);

  return 0;
}


