
#ifndef __GIF_H__
#define __GIF_H__

#include "string.h"
#include "stdlib.h"
#include "stdint.h"
#include "stdio.h"
#include "math.h"

/* LABELS and FLAGS definition  */
#define SIGNATURE "GIF"
#define V_87a "87a"
#define V_89a "89a"
#define TERMINATOR 0x00
#define IMG_SEPAR 0x2c
#define EXT_INTRO 0x21
// Graphical Control Extension
#define GCE       0xf9 
#define GCE_BSIZE 0x04
// Comment Extension
#define CE        0xfe
// Plain Text Extension
#define PTE       0x01
#define PTE_BSIZE 0x0c
// Application Extension
#define AE        0xff
#define AE_BSIZE  0x0b

#define TRAILER 0x3b

/* LSD FLAGS MASKS */
#define LSD_GCTF 0x80
#define LSD_CR   0x70
#define LSD_SF   0x08
#define LSD_SGCT 0x07
/* ID FLAGS MASKS */
#define ID_LCTF  0x80
#define ID_IF    0x40
#define ID_SF    0x20
#define ID_RES   0x18
#define ID_SLCT  0x07
/* GCE FLAGS MASKS */
#define GCE_RES  0xE0
#define GCE_DIS  0x1c
#define GCE_UIF  0x02
#define GCE_TCF  0x01

/* SHARED MASKS */
#define SHA_CTF 0x80
#define SHA_SCT 0x07

/* DECODING STRUCTURE */
typedef struct {
  int64_t bmpSize;
  int64_t gifSize;
} tGIF2BMP;

/*  ERROR CODES  */
enum err_code {
  OK = 0,
  ERR = 1,
  NIM,
  EFILE,
  EEOF,
  EHEAD,
  ELSD,
  EID,
  EIMD,
  EPTE,
  EAE,
  ECE,
  EGCE,
  EMEM,
  EBMP,
  EPAL
};

/* COLOR palette structure */
typedef struct {
  uint8_t size;
  uint32_t color[256];
} palette_t;

typedef uint32_t * ibuf_t; // image buffer
typedef uint8_t lsdf_t; // logical screen descriptor flag
typedef uint8_t idf_t; // image descriptor flag
typedef uint8_t gcef_t; // graphics control extension flag

/* GIF HEAD */
typedef struct {
  uint8_t version[3];
} gifhead_t;

/* GIF Logical Screen Descriptor */
typedef struct {
  uint16_t width;
  uint16_t height;
  uint8_t  flags;
  uint8_t  backIndex;
  uint8_t  aspectRatio;
} giflsd_t;

/* GIF Image Descriptor */
typedef struct {
  uint16_t left;
  uint16_t top;
  uint16_t width;
  uint16_t height;
  uint8_t flags;
  palette_t lct;
} gifid_t;

/* GIF Graphics Control Extension */
typedef struct {
  uint8_t size;
  uint8_t flags;
  uint16_t delay;
  uint8_t tranparent;
} gifgce_t;

/* GIF PlainTextExtension */
typedef struct {
  uint8_t size;
  uint16_t left;
  uint16_t top;
  uint16_t width;
  uint16_t height;
  uint8_t cellWidth;
  uint8_t cellHeight;
  uint8_t foreIndex;
  uint8_t backIndex;
} gifpte_t;

/* LZW Dictionary word */
typedef struct {
  uint16_t ptr;
  uint8_t idx;
} lzword_t;

/*  LZW DICTIONARY  */
typedef lzword_t lzwdict_t[4096]; 

/* basic decoding interface */
int gif2bmp(tGIF2BMP * gif2bmp, FILE * inputFile, FILE * outputFile);

int parseGifHead(FILE * data, giflsd_t * lsd);
int parseGifLSD(FILE * data, giflsd_t * lsd);
int parsePalette(FILE * data, palette_t * ct, const lsdf_t flags);
int parseImageDescriptor(FILE * data, gifid_t * id);
int parseImageData(FILE * data, ibuf_t img, const giflsd_t * lsd, gifid_t * id,
                   palette_t * gct, gifgce_t * gce); // comes after parseImageDescriptor

int parsePlainTextExtenstion(FILE * data, ibuf_t img, const giflsd_t * lsd);
int parseApplicationExtension(FILE * data);
int parseCommentExtension(FILE * data);
int parseGraphicalControlExtension(FILE * data, gifgce_t * gce);

/* get a 3-12 bit patterns from dataBlocks
 * data - input file
 * bit - number of bits to read
 * len - counter of subblock remaining Bytes
 * reset - bool to reset, 0 - not reset (do reset in every ID)
 * */
uint16_t getLZWChar(FILE * data, uint8_t bit, uint8_t * len, uint8_t * reset);


/* UTILS */
uint32_t convColor(uint8_t b0, uint8_t b1, uint8_t b2);
uint16_t conv2Byte(uint8_t b0, uint8_t b1); 
uint8_t conv1Byte(uint8_t b0); 

void initPalette(palette_t * ct);
/* set proper palette size from LSD/ID flag */
void getPaletteSize(palette_t * ct, const uint8_t flags);

/* init a dictionary by pattern size
 * bit - current pattern size
 * */
void initDict(lzwdict_t dict, uint8_t bit);

/* translate a pattern by dictionary, and properly output to image buffer,
 * supports local palettes, interleaving
 * img - imageBuffer for picture data
 * begin - pattern to translate
 * buffer - word reversing buffer -> for speedup one creation per ID
 * dict - current LZW dictionary
 * lsd - current LSD
 * id - current ID
 * gct - current Global Color Table
 * reset - bool for reseting rendering, 0 - no reset (do reset in every ID)
 * */
void output(ibuf_t img, uint16_t begin, uint8_t * buffer, const lzwdict_t dict,
            const giflsd_t * lsd, const gifid_t * id, const palette_t * gct,
            uint8_t * reset);

/* lookup for first character in pattern string */
uint8_t getFirstChar(uint16_t begin, const lzwdict_t dict);

/* render BMP to file */
int pushBMP(FILE * data, ibuf_t img, giflsd_t * lsd);

/* Error handler */
int error(int type);

/* create 256 color monochrome palette if no GCT in image present*/
void defaultPalette(palette_t * ct);


/* DEBUG */
void printGifLSD(const giflsd_t * lsd);
void printGifID(const gifid_t * id);
void printGifPalette(const palette_t * ct);
void printDictionary(lzwdict_t dict, uint16_t last);

#endif

