
#include "gif2bmp.h"


int gif2bmp(tGIF2BMP * gif2bmp, FILE * inputFile, FILE * outputFile) {
  giflsd_t lsd;
  gifid_t  id;
  palette_t gct;
  gifgce_t gce;
  ibuf_t imgBuffer = NULL;


  memset(&lsd, 0, sizeof(giflsd_t)); // reset values
  memset(&id, 0, sizeof(gifid_t)); // reset values
  memset(&gce, 0, sizeof(gifgce_t)); // reset values
  initPalette(&gct);

  if (inputFile == NULL) {
    return error(EFILE);
  }
  if (outputFile == NULL) {
    return error(EFILE);
  }

  /*  HEAD  */
  if (parseGifHead(inputFile, &lsd) != OK) {
    return error(EHEAD);
  }

  /*  Logical Screen Descriptor  */
  if (parseGifLSD(inputFile, &lsd) != OK) {
    return error(ELSD);
  }

  // prepare buffer
  imgBuffer = (ibuf_t) malloc(sizeof(uint32_t) * lsd.height * lsd.width);
  if (imgBuffer == NULL) {
    return error(EMEM);
  }
  memset(imgBuffer, 0xffffffff, sizeof(uint32_t) * lsd.height * lsd.width);

  /*  Global Color Table  */
  if ((lsd.flags & SHA_CTF) != 0) {
    getPaletteSize(&gct, lsd.flags);
    if (parsePalette(inputFile, &gct, lsd.flags) != OK) {
      free(imgBuffer);
      return error(EPAL);
    }
  } else {
    defaultPalette(&gct);
  }


  /*  <Data>*  */
  uint8_t decision;
  if (fread(&decision, sizeof(uint8_t), 1, inputFile) != 1) {
      free(imgBuffer);
    return error(EEOF);
  }

  while (decision != TRAILER) { // untill file end
    switch (decision) {
      case IMG_SEPAR:  /* Table Based Image */
        /*  Image Desriptor + [Local Color Table] */
        if (parseImageDescriptor(inputFile, &id) != OK) {
          free(imgBuffer);
          return error(EID);
        }

        // boundaries check
        if ((id.left+id.width) > lsd.width || (id.top+id.height) > lsd.height) {
          fprintf(stderr, "SubImage exceeds Logical Screen Descriptor's range\n");
          free(imgBuffer);
          return error(EID);
        }

        /* parse current image data */
        if (parseImageData(inputFile, imgBuffer, &lsd, &id, &gct, &gce) != OK) {
          free(imgBuffer);
          return error(EIMD);
        }
        break;
      case EXT_INTRO:
        if (fread(&decision, sizeof(uint8_t), 1, inputFile) != 1) {
          free(imgBuffer);
          return error(EEOF);
        }
        switch (decision) {
          case PTE:
            parsePlainTextExtenstion(inputFile, imgBuffer, &lsd);
            break;
          case GCE:
            parseGraphicalControlExtension(inputFile, &gce);
            break;
          case CE:
            parseCommentExtension(inputFile);
            break;
          case AE:
            parseApplicationExtension(inputFile);
            break;
          default:
            fprintf(stderr, "extension selection\n");
            free(imgBuffer);
            return error(ERR);
            break;
        }
      break;

      default:
        fprintf(stderr, "unknown mark\n"); 
        free(imgBuffer);
        return error(ERR);
        break;
    }
    if (fread(&decision, sizeof(uint8_t), 1, inputFile) != 1) {
      free(imgBuffer);
      return error(EEOF);
    }
  }

  // render image
  pushBMP(outputFile, imgBuffer, &lsd);

  // evaluate file sizes
  gif2bmp->bmpSize = ftell(outputFile);
  gif2bmp->gifSize = ftell(inputFile);

  // cleanup
  free(imgBuffer);

/*
  printGifLSD(&lsd);
  printf(" * GLOBAL ");
  printGifPalette(&gct);
  printGifID(&id);
*/
  return OK;
}



int parseGifHead(FILE * data, giflsd_t * lsd) {
   uint8_t buffer[6];

  fread(buffer, sizeof(uint8_t), 6, data);

  // GIF label
  if (buffer[0] != 'G' || buffer[1] != 'I' || buffer[2] != 'F') {
    fprintf(stderr, "bad GIF ID");
    return ERR;
  }
  // version checking
  if (buffer[3] != '8' || buffer[5] != 'a') { // shared version string
    if (buffer[4] != '9' || buffer[4] != '7') { // dedicated version string
    fprintf(stderr, "bad GIF Version");
    return ERR;
    }
  }

  return OK;
}

int parseGifLSD(FILE * data, giflsd_t * lsd) {
   uint8_t buffer[7];

  if(fread(buffer, sizeof(uint8_t), 7, data) != 7) {
    return error(EEOF);
  } 

  lsd->width = conv2Byte(buffer[0], buffer[1]);
  lsd->height = conv2Byte(buffer[2], buffer[3]);

  lsd->flags = conv1Byte(buffer[4]);
  lsd->backIndex = conv1Byte(buffer[5]);
  lsd->aspectRatio = conv1Byte(buffer[6]);

  return OK;
}

int parsePalette(FILE * data, palette_t * ct, const lsdf_t flags) {
  char buffer[8]; 

  uint16_t size = ct->size;
  // if size estimated to 0 -> 256 colors, presence of CT is given by flags
  if (ct->size == 0) { 
    size = 256;
  }

  for (uint16_t i = 0; i < size; i++) {
    fread(buffer, sizeof(char), 3, data);
    ct->color[i] = convColor(buffer[0], buffer[1], buffer[2]);
  }
  
  return OK;
}

int parseImageDescriptor(FILE * data, gifid_t * id) {
  uint8_t buffer[10];

  buffer[0] = IMG_SEPAR;
  // 0x2c has been readed
  if(fread(buffer+sizeof(uint8_t), sizeof(uint8_t), 9, data) != 9) { 
    return error(EEOF);
  } 

  id->left = conv2Byte(buffer[1], buffer[2]); 
  id->top = conv2Byte(buffer[3], buffer[4]); 
  id->width = conv2Byte(buffer[5], buffer[6]);
  id->height = conv2Byte(buffer[7], buffer[8]);

  id->flags = conv1Byte(buffer[9]);

  /*  Local Color Table  */
  if ((id->flags & SHA_CTF) != 0) {
    getPaletteSize(&(id->lct), id->flags);
    parsePalette(data, &(id->lct), id->flags);
  }

  return OK;
}

int parseImageData(FILE * data, ibuf_t img, const giflsd_t * lsd, 
                   gifid_t * id, palette_t * gct, gifgce_t *gce) {
  uint8_t bit = 0; // size of patterns of bits
  uint8_t startbit = 0; // initial size of pattern
  uint8_t len = 0; // count of data block remaining Bytes
  uint8_t buffer[256]; // working buffer 

  uint8_t reset = 1; // do reset LZW reading counters
  uint8_t resetOutput = 1; // do reset Rendering counters

  uint16_t stop = 0; // Stop Code (EOI)
  uint16_t clear = 0; // Clear Code

  uint16_t new = 0xffff; // new pattern buffer
  uint16_t old = 0xffff; // old pattern buffer
  uint16_t last = 0; // top of dictionary -> first empty value
  uint8_t ch = 0xff; // last character for LZW

  lzwdict_t dict; // dictionary
  uint8_t wordBuffer[4096]; // reversing buffer

  // security reset 
  memset(wordBuffer, 0, sizeof(uint8_t) * 4096);


  if(fread(&bit, sizeof(uint8_t), 1, data) != 1) { // LZW char bit count
    return error(EEOF);
  }

  // estimating LZW
  clear = pow(2, bit);
  stop = clear + 1;
  last = stop + 1;

  if (bit < 2) {
    return error(EID);
  }

  startbit = bit;
  bit++;
/*
  if(fread(&len, sizeof(uint8_t), 1, data) != 1) { // LZW char bit count
    return error(EEOF);
  }
*/
  //printf("b: %02x\nl: %02x\n", bit, len);
  //printf("c: %04x\ns: %04x\n", clear, stop);

/*
  if(fread(buffer, sizeof(uint8_t), len, data) != len) { // LZW char bit count
    return error(EEOF);
  }
*/

  //printf("%p\n",(void *) &dict);

  while (len > 0 || new != stop) { // reading until LZW stop
    new = getLZWChar(data, bit, &len, &reset); // new pattern 
   //printf("n: %04x o: %04x\n", new, old);

/*
    printf("o|");
    for (uint8_t i = bit; i > 0; i--) {
      printf("%x", (old >> (i-1)) & 0x01);
    }
    printf("| n|");
    for (uint8_t i = bit; i > 0; i--) {
      printf("%x", (new >> (i-1)) & 0x01);
    }
    printf("| c:%02x", ch);
    printf("\n");

    printDictionary(dict, last);
*/
    if (new == 0xFFFF) { // error in reading
      return error(EEOF);
    } else if (new == clear) { // clear code
      initDict(dict, startbit);
      old = 0xffff;
      ch = 0xff;
      bit = startbit + 1;
      last = stop + 1;
      continue;
    } else if (new == stop) { // stop code
      continue;
    }

    if (old == 0xffff) { // initializing old values -> output character
      old = new;
      ch = dict[new].idx;
      output(img, new, (uint8_t *)&wordBuffer, dict, lsd, id, gct, &resetOutput);
      continue;
    }
   
  

    if (new >= last) { //not in dictionary -> translate old and add char, and refresh
      output(img, old, (uint8_t *)&wordBuffer, dict, lsd, id, gct, &resetOutput);
      output(img, ch, (uint8_t *)&wordBuffer, dict, lsd, id, gct, &resetOutput);
      ch = getFirstChar(old, dict);
      //printf ("N");
    } else { // in dict -> translate new, refresh char
      ch = getFirstChar(new, dict);
      //ch = dict[new].idx;
      output(img, new, (uint8_t *)&wordBuffer, dict, lsd, id, gct, &resetOutput);
    }

   //printf("%02d  ", bit);

      if (last < 4096) { // is room in dict -> add string 
        dict[last].ptr = old;
        dict[last].idx = ch;
      //printf ("O");
        last++;
      }
    old = new; // renew
    
    //printf("l:%02x ", last-1);
    
   /* if dictionary is filled up, but no bit limit reached */ 
    if (last == pow(2,bit) && bit < 12) {
      bit++; // -> increasing pattern size
    }

  }
  while (len > 0) { // if data in subblock remaining Security
    if(fread(buffer, sizeof(uint8_t), len, data) != len) { 
      return error(EEOF);
    }
    len--;
  }

  if(fread(&len, sizeof(uint8_t), 1, data) != 1) {  // expecting TERMINATOR
    return error(EEOF);
  }
  if (len != TERMINATOR) {
    return error(EID);
  }

  //printf("\n");

  return OK;
}

// Parsing but not used yet
int parsePlainTextExtenstion(FILE * data, ibuf_t img, const giflsd_t * lsd) {
  gifpte_t pte;
  uint8_t len;
  uint8_t buffer[256];

  if(fread(&buffer, sizeof(uint8_t), 13, data) != 13) { 
    return error(EEOF);
  }

  pte.size = buffer[0];
  pte.left = conv2Byte(buffer[1], buffer[2]);
  pte.top = conv2Byte(buffer[3], buffer[4]);
  pte.width = conv2Byte(buffer[5], buffer[6]);
  pte.height = conv2Byte(buffer[7], buffer[8]);
  pte.cellWidth = buffer[9];
  pte.cellHeight = buffer[10];
  pte.foreIndex = buffer[11];
  pte.backIndex = buffer[12];


  if(pte.size != PTE_BSIZE) {
    return error(EPTE);
  }


  if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
    return error(EEOF);
  }

  if (len == 0x00) {
    return error(EPTE);
  }

  while (len != 0) { 
    if(fread(buffer, sizeof(uint8_t), len, data) != len) { 
      return error(EEOF);
    }
    if (len == 255) {
      if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
        return error(EEOF);
      }
    } else { 
      break; 
    }
  }

  if (len != TERMINATOR) {
    if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
      return error(EEOF);
    }
    if (len != TERMINATOR) {
      return error(EPTE);
    }
  }

  return OK;
}

// Skipping not used yet
int parseApplicationExtension(FILE * data) {
  uint8_t len;
  uint8_t buffer[256];

  if(fread(&buffer, sizeof(uint8_t), 12, data) != 12) { 
    return error(EEOF);
  }

  if(buffer[0] != AE_BSIZE) {
    return error(EAE);
  }


  if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
    return error(EEOF);
  }

  if (len == 0x00) {
    return error(EAE);
  }

  while (len != 0) { 
    if(fread(buffer, sizeof(uint8_t), len, data) != len) { 
      return error(EEOF);
    }
    if (len == 255) {
      if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
        return error(EEOF);
      }
    } else { 
      break; 
    }
  }

  if (len != TERMINATOR) {
    if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
      return error(EEOF);
    }
    if (len != TERMINATOR) {
      return error(EAE);
    }
  }

  return OK;

}

// Skipping not used yet
int parseCommentExtension(FILE * data) {
 uint8_t len;
 uint8_t buffer[256];

  if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
    return error(EEOF);
  }

  if (len == 0x00) {
    return error(ECE);
  }

  while (len != 0) { 
    if(fread(buffer, sizeof(uint8_t), len, data) != len) { 
      return error(EEOF);
    }
    if (len == 255) {
      if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
        return error(EEOF);
      }
    } else { 
      break; 
    }
  }

  if (len != TERMINATOR) {
    if(fread(&len, sizeof(uint8_t), 1, data) != 1) { 
      return error(EEOF);
    }
    if (len != TERMINATOR) {
      return error(EGCE);
    }
  }

  return OK;
}

// Parsing but not used yet
int parseGraphicalControlExtension(FILE * data, gifgce_t * gce) {
  uint8_t buffer[6];

  if(fread(&buffer, sizeof(uint8_t), 6, data) != 6) { 
    return error(EEOF);
  }

  gce->size = buffer[0];
  gce->flags = buffer[1];
  gce->delay = conv2Byte(buffer[2], buffer[3]);
  gce->tranparent = buffer[4];

  if(gce->size != GCE_BSIZE) {
    return error(EGCE);
  }

  if(buffer[5] != TERMINATOR) {
    return error(EGCE);
  }

  return OK;
}




uint16_t getLZWChar(FILE * data, uint8_t bit, uint8_t * len, uint8_t * reset) {
  static uint8_t buffer = 0; // reading buffer
  static uint8_t cnt = 0; // good bits counter
  uint16_t out = 0; // output buffer
 
  if (*reset != 0) { 
    buffer = 0;
    cnt = 0;
    *reset = 0;
  }

  // For needing bits
  for (uint8_t i = bit; i > 0; i--) {
    if (*len == 0 && cnt == 0) { // end of subblock -> get new one
      if(fread(len, sizeof(uint8_t), 1, data) != 1) {  // length
        return 0xFFFF;
      }
      //printf("L: %02x\n", *len);
    }
    if (cnt == 0) { // no good bits in buffer
      if(fread(&buffer, sizeof(uint8_t), 1, data) != 1) { 
        return 0xFFFF;
      }
      //printf("%02x\n", buffer);
      cnt = 8;
      (*len)--;
    }
    // Get a bit from buffer and put into output buffer
    // folowed but outputbuffer current content
    out = ((uint16_t)(buffer & 0x01) << (bit-i)) + out;
    buffer >>= 1; // remove a bit from buffer
    cnt--; // decrease goot bit count
  }

  return out;
}

uint32_t convColor(uint8_t b0, uint8_t b1, uint8_t b2) {
  uint32_t tmp = 0;

  tmp = b2 & 255;
  tmp <<= 8; 
  tmp += b1 & 255;
  tmp <<= 8; 
  tmp += b0 & 255;

  return tmp;

}

uint16_t conv2Byte(uint8_t b0, uint8_t b1) {
  uint16_t tmp = 0;

  tmp = b1 & 255;
  tmp <<= 8; // rotate to end of tmp
  tmp += b0 & 255; // mask higher 8 bits

  return tmp;
}

uint8_t conv1Byte(uint8_t b0) {
  uint8_t tmp = 0;

  tmp = b0 & 255; // mask higher 8 bits

  return tmp;
}

void initPalette(palette_t * ct) {
   memset(ct, 0, sizeof(palette_t));
   return;
}

void getPaletteSize(palette_t * ct, const uint8_t flags) {
  uint8_t tmp;
    tmp = (flags & SHA_SCT);
    tmp = pow(2, tmp+1);
    ct->size = tmp;
  return;
}

void initDict(lzwdict_t dict, uint8_t bit) {
  uint16_t stop = pow(2, bit);
  //printf("%p\n", (void *) dict);
  memset(dict, 0xff, sizeof(lzword_t)*4096); // reset all

  for (uint16_t i = 0; i < stop; i++) { // set first values to indexes to palette
    dict[i].idx = i;
  }

  return;
}

void output(ibuf_t img, uint16_t begin, uint8_t * buffer, const lzwdict_t dict,
            const giflsd_t * lsd, const gifid_t * id, const palette_t * gct,
            uint8_t * reset) {
  static uint16_t x = 0; // x coordinate 
  static uint16_t y = 0; // y coordinate, static to current position
  uint8_t g1 = ceil(lsd->height/8); // interleaving groups
  uint8_t g2 = ceil(lsd->height/4);
  uint8_t g3 = ceil(lsd->height/2);
  uint16_t bufptr = 0; // reverse buffer pointer
  uint16_t tmp = begin; // beginning pattern
  uint64_t offset = 0; // rendering computed offset to imgBuffer
  palette_t * act; // current selected palette

  if (*reset != 0) {
    x = 0;
    y = 0;
    *reset = 0;
    g1 = ceil(lsd->height/8);
    g2 = ceil(lsd->height/4);
    g3 = ceil(lsd->height/2);
  }

  if ((id->flags & SHA_CTF) != 0) { // palette selection
    act = (palette_t *)&(id->lct);
  } else {
    act = (palette_t *)gct;
  }

  //printf(".");
  buffer[bufptr] = dict[tmp].idx; // translate a string from dictionary
  while (dict[tmp].ptr != 0xFFFF) { // walkthrough dictionary by parent index
    bufptr++;
    tmp = dict[tmp].ptr;
    buffer[bufptr] = dict[tmp].idx; // reversing buffer
    //printf("*");
  }
  //printf("\n");


  // rendering from reverse buffer
  while(bufptr != 0xffff) {
    //printf("ptr: %04x\n", bufptr);
    if (y < id->height) { // boundary check SECURE
    //printf(".");
      //printf("%06x\n", act->color[buffer[bufptr]]); // TODO ACT
      if ((id->flags & ID_IF) == 0) { // without interleaving
        offset = (y+id->top)*id->width + (x+id->left);
      } else {

        // interleaving group selection and offset computation
        if (y < g1) {
          offset = (y*8+0+id->top)*id->width + (x+id->left);
        } else if (y < g2) {
          offset = ((y-g1)*8+4+id->top)*id->width + (x+id->left);
        } else if (y < g3) {
          offset = ((y-g2)*4+2+id->top)*id->width + (x+id->left);
        } else {
          offset = ((y-g3)*2+1+id->top)*id->width + (x+id->left);
        }

      }
      *(img + offset) = act->color[buffer[bufptr]]; // RENDER a pixel
      //printf("[%02d, %02d] ", x, y);
    } else {// silently discards
      return;
    }
    //printf("x");
    
    /* SAFELY INCREASE OF COORDINATES */
    if (x == id->width - 1) {
      x = 0; // new line
      if (y == id->height - 1) {
        y++;
        continue; // silently discards
      }
      y++;
    } else {
      x++; // new column
    }

    bufptr--; // pop from reverse buffer
  }
  

  return;
}


uint8_t getFirstChar(uint16_t begin, const lzwdict_t dict) {
  uint8_t ch = 0;
  uint16_t tmp = begin;


  ch = dict[tmp].idx;
  while (dict[tmp].ptr != 0xFFFF) { // dictionary walkthrough by parent pointer
    tmp = dict[tmp].ptr;
    ch = dict[tmp].idx;
  }

  return tmp;
}


int pushBMP(FILE * data, ibuf_t img, giflsd_t * lsd) {
  uint8_t buf8 = 0;  // output buffers
  uint16_t buf16 = 0;
  uint32_t buf32 = 0;

  uint64_t size = lsd->width*lsd->height; // size of image for tranposition

  uint8_t buffer[3]; // color buffer

  uint8_t padding = (4 - ((lsd->width*3)%4))%4; // compute bmp padding
  buf8 = 0x00; 

  uint64_t idx = 0; // output index
  uint64_t x = 0; // coords
  uint64_t y = lsd->height-1;

/* ##### BMP partially static HEADER ##### */

  /* MAGIC NUMBER */
  buf16 = 0x4d42; // BM
  if (fwrite(&buf16, sizeof(uint16_t), 1, data) != 1) {
    return error(EBMP);
  }

  /* size */
  buf32 = 3*8*lsd->width*lsd->height + 0x36; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }

  /* APP */
  buf16 = 0x0000; 
  if (fwrite(&buf16, sizeof(uint16_t), 1, data) != 1) {
    return error(EBMP);
  }
  /* APP */
  buf16 = 0x0000; 
  if (fwrite(&buf16, sizeof(uint16_t), 1, data) != 1) {
    return error(EBMP);
  }


  /* OFF */
  buf32 = 0x0036; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }
 /* HEAD SIZE */
  buf32 = 0x0028; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }


 /* width */
  buf32 = lsd->width; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }

 /* height */
  buf32 = lsd->height; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }

/* planes */
  buf16 = 0x0001; 
  if (fwrite(&buf16, sizeof(uint16_t), 1, data) != 1) {
    return error(EBMP);
  }

/* BPP */
  buf16 = 0x0018; 
  if (fwrite(&buf16, sizeof(uint16_t), 1, data) != 1) {
    return error(EBMP);
  }
 
 /* compression */
   buf32 = 0x00000000; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }


  /* size */
  buf32 = 3*lsd->width*lsd->height + padding*lsd->height; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }

  /* horisontal res */
  buf32 = 0x0000130b; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }  /* vertical res */
  buf32 = 0x0000130b; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }

  /* palette */
  buf32 = 0x00000000; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }  
  /* importatnt colors */
  buf32 = 0x00000000; 
  if (fwrite(&buf32, sizeof(uint32_t), 1, data) != 1) {
    return error(EBMP);
  }

 
/* ##### END OF BMP HEADER ##### */

  buf8 = 0xff;
    //printf("rendering\n");
  for (uint64_t i = 0; i < size; i++) {
    //printf("[%02d, %02d]\n", x, y);
    idx = y*(lsd->width) + x;// index computation
    buffer[0] = (img[idx] >> 16) & 0xff;
    buffer[1] = (img[idx] >> 8) & 0xff;
    buffer[2] = (img[idx]) & 0xff;
    if (fwrite(&buffer, sizeof(uint8_t), 3, data) != 3) { // render
      return error(EBMP);
    }
    if (x != 0 && x%(lsd->width-1) == 0) { // padding if needed
      for (uint8_t p = 0; p < padding; p++) {
        if (fwrite(&buf8, sizeof(uint8_t), 1, data) != 1) {
          return error(EBMP);
        }
      }
    }

    /* SAFELY INCREASE/DECREASE OF COORDINATES */
    if (x == lsd->width - 1) {
      x = 0;
      y--;
    } else {
      x++;
    }


  }



  return OK;
}



/* Error handler */
int error(int type) {
  switch (type) {
    case OK: 
      break;
    case ERR:
      fprintf(stderr, "error");
      break;
    case NIM:
      fprintf(stderr, "Not Implemented.");
      break;
    case EFILE:
      fprintf(stderr, "Could not open file");
      break;
    case EEOF:
      fprintf(stderr, "unexpected end of file");
      break;
    case EHEAD:
      fprintf(stderr, "error in parsing head");
      break;
    case ELSD:
      fprintf(stderr, "error in parsing Logical Screen Descriptor");
      break;
    case EID:
      fprintf(stderr, "error in parsing Image Descriptor");
      break;
    case EIMD:
      fprintf(stderr, "error in parsing Image Data");
      break;
    case EPTE:
      fprintf(stderr, "error in parsing Plain Text Extension");
      break;
    case EAE:
      fprintf(stderr, "error in parsing Application Extension");
      break;
    case ECE:
      fprintf(stderr, "error in parsing Comment Extension");
      break;
    case EGCE:
      fprintf(stderr, "error in parsing Graphical Control Extension");
      break;
    case EMEM:
      fprintf(stderr, "error memmory allocation");
      break;
    case EBMP:
      fprintf(stderr, "error pushing BMP");
      break;
    case EPAL:
      fprintf(stderr, "error parsing palette");
      break;

    default:
      fprintf(stderr, "unknown error");
      break;
  } 
  
  fprintf(stderr, "\n");


  return type;
}


void defaultPalette(palette_t * ct) {
  ct->size = 0x00;

  ct->color[0] = 0x00000000; // black
  ct->color[1] = 0x00ffffff; // white

  uint32_t tmp = 0x00010101;
  for(uint16_t i = 2; i < 0x0100; i++) {
    ct->color[i] = tmp;
    tmp += 0x00010101;
  }

  return;
}


void printGifLSD(const giflsd_t * lsd) {
  printf("-- Logical Screen Descriptor --\n");
  printf("w: %04x\nh: %04x\n", lsd->width, lsd->height);
  printf("f: %02x ", lsd->flags);
  printf("|%x.%x%x%x.%x.%x%x%x|\n",
        (lsd->flags & 0x80) >> 7,
        (lsd->flags & 0x40) >> 6,
        (lsd->flags & 0x20) >> 5,
        (lsd->flags & 0x10) >> 4,
        (lsd->flags & 0x08) >> 3,
        (lsd->flags & 0x04) >> 2,
        (lsd->flags & 0x02) >> 1,
        (lsd->flags & 0x01));
  printf("b: %02x\na: %02x\n", lsd->backIndex, lsd->aspectRatio);

  return;
}

void printGifID(const gifid_t * id) {
  printf("-- Image Descriptor --\n");
  printf("t: %04x\nl: %04x\n", id->top, id->left);
  printf("w: %04x\nh: %04x\n", id->width, id->height);
  printf("f: %02x ", id->flags);
  printf("|%x.%x%x%x.%x.%x%x%x|\n",
        (id->flags & 0x80) >> 7,
        (id->flags & 0x40) >> 6,
        (id->flags & 0x20) >> 5,
        (id->flags & 0x10) >> 4,
        (id->flags & 0x08) >> 3,
        (id->flags & 0x04) >> 2,
        (id->flags & 0x02) >> 1,
        (id->flags & 0x01));
  printf(" * LOCAL ");
  printGifPalette(&(id->lct));
  return;
}

void printGifPalette(const palette_t * ct) {


  uint16_t size = ct->size;
  if (ct->size == 0) {
    size = 256;
  }

  printf("Color Table --\n");
  printf("s: %02x\n", size);
  for (int i = 0; i < size; i++) {
    printf("%d: %02x %02x %02x\n", 
        i,
        (ct->color[i] >> 16) & 255, 
        (ct->color[i] >> 8) & 255, 
        (ct->color[i] & 255));
  }
  return;
}


void printDictionary(lzwdict_t dict, uint16_t last) {

printf("   INDEX    |  CHAR  |    CODE    \n");

for (uint16_t i = 0; i < last; i++) {

  for (uint8_t y = 12; y > 0; y--) {
    printf("%x", (i >> (y-1)) & 0x01);
  }
  printf("|");
  for (uint8_t y = 8; y > 0; y--) {
    printf("%x", (dict[i].idx >> (y-1)) & 0x01);
  }
  printf("|");
  for (uint8_t y = 12; y > 0; y--) {
    printf("%x", (dict[i].ptr >> (y-1)) & 0x01);
  }
  printf("\n");
}
printf("--------------------------\n");

return;
}


