#include "cstr_int.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"

/* --- Private --- */

cstr_t *
cstr_spawn(unsigned int length) {
  cstr_t * tmp = (cstr_t*) calloc(1, sizeof(cstr_t));
  if (tmp == NULL) {
    return NULL;
  }

  if (length < CSTR_INIT_CAPACITY) {
    length = CSTR_INIT_CAPACITY;
  }
  length++; // addition space for \0

  tmp->data = (char*) calloc(1, sizeof(char) * length);
  if (tmp->data == NULL) {
    return NULL;
  }
  tmp->capacity = length;

  return tmp;
}

bool
cstr_valid(cstr_t* cstr) {
  if (cstr == NULL) return false;
  if (cstr->data == NULL) return false;
  if (cstr->length >= cstr->capacity) return false;
  return true;
}

void
cstr_free(cstr_t* cstr) {
  if (!cstr_valid) return;
  free(cstr->data);
  cstr->data = NULL;
  free(cstr);
  cstr = NULL;
  return;
}

bool
cstr_resize(cstr_t* cstr, unsigned int length) {
  if (!cstr_valid(cstr)) return false;
  if (length <= cstr->capacity) return true;

  length++; // addition space for \0

  char* new_data = realloc(cstr->data, sizeof(char*) * length);
  if (new_data == NULL) return false;
  cstr->data = new_data;
  cstr->capacity = length;
  return true;
}

void
str_copy(void* dest, unsigned int di, void* src, unsigned int length) {
  void* x_dest = dest + di * sizeof(char);
  size_t y_len = length * sizeof(char);
  memcpy(x_dest, src, y_len);
}

