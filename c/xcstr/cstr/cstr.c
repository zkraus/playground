#include "cstr.h"
#include "cstr_int.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "stdbool.h"



/* --- Public --- */

unsigned int
str_length(char * data) {
  unsigned int len = 0;
  while (*(data + len) != '\0') len++;
  return len;
}

cstr_t*
cstr(char* data) {
  unsigned int length = str_length(data);
  cstr_t * tmp = cstr_spawn(length);
  if (tmp == NULL) {
    return NULL;
  }
  str_copy(tmp->data, 0, data, length);
  tmp->data[length] = '\0';
  tmp->length = length;
  return tmp;
}

void
cstr_del(cstr_t* cstr) {
  cstr_free(cstr);
}

void
cstr_concat_in(cstr_t* a, cstr_t* b) {
  if (!(cstr_valid(a) && cstr_valid(b))) return;
  unsigned int length = a->length + b->length;
  cstr_resize(a, length);
  str_copy(a->data, a->length, b->data, b->length);
  a->data[length] = '\0';
  a->length = length;
  return;
}

void
cstr_strcat_in(cstr_t* a, char* b) {
  if (!cstr_valid(a)) return;
  unsigned int b_length = str_length(b);
  unsigned int length = a->length + b_length;
  cstr_resize(a, length);
  str_copy(a->data, a->length, b, b_length);
  a->data[length] = '\0';
  a->length = length;
  return;

}

cstr_t*
cstr_concat(cstr_t* a, cstr_t* b) {
  if (!(cstr_valid(a) && cstr_valid(b))) return NULL;
  unsigned int length = a->length + b->length;
  cstr_t* new = cstr_spawn(length);
  str_copy(new->data, 0, a->data, a->length);
  str_copy(new->data, a->length, b->data, b->length);
  new->length = length;
  return new;
}

cstr_t*
cstr_strcat(cstr_t* a, char* b) {
  if (!cstr_valid(a)) return NULL;
  unsigned int b_length = str_length(b);
  unsigned int length = a->length + b_length;
  cstr_t* new = cstr_spawn(length);
  str_copy(new->data, 0, a->data, a->length);
  str_copy(new->data, a->length, b, b_length);
  new->length = length;
  return new;
}


int
cstr_cmp(cstr_t* a, cstr_t* b) {
  int result = 0;
  int len_result = 0;

  unsigned int i_max = a->length;
  if (b->length > a->length) i_max = b->length;
  len_result = a->length - b->length;

  unsigned int i = 0;
  while (i < i_max) {
    char ia = *(a->data+i*sizeof(char));
    char ib = *(b->data+i*sizeof(char));
    if (ia != ib) {
      if (ia > ib) result = 1;
      else result = -1;
      break;
    }
    i++;
  }
  if (result == 0) result += len_result;
  return result;
}



void cstr_print(cstr_t* cstr, char* name) {
  if (name != NULL) {
    printf("CSTR(%s)::len=%d,cap=%d,data=%s\n", name, cstr->length, cstr->capacity, cstr->data);
  } else {
    printf("CSTR::len=%d,cap=%d,data=%s\n", cstr->length, cstr->capacity, cstr->data);
  }
}



// eof
