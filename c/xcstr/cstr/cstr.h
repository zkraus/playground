#ifndef __CSTR_H__
#define __CSTR_H__

#define CSTR_INIT_CAPACITY 15
#define CSTR_ADD_CAPACITY 8

typedef struct cstr_s {
  unsigned int length;
  unsigned int capacity;
  char* data;
} cstr_t;

unsigned int
str_length(char *);

cstr_t* cstr(char *);
void cstr_del(cstr_t *);

void cstr_concat_in(cstr_t*, cstr_t*);
void cstr_strcat_in(cstr_t*, char*);
cstr_t* cstr_concat(cstr_t*, cstr_t*);
cstr_t* cstr_strcat(cstr_t*, char*);

int cstr_cmp(cstr_t*, cstr_t*);

void cstr_print(cstr_t*, char*);


#endif
