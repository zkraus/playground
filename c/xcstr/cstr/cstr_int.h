#ifndef __CSTR_INT_H__
#define __CSTR_INT_H__

#include "cstr.h"
#include "stdbool.h"

/* --- internal declarations --- */
cstr_t* cstr_spawn(unsigned int);

bool cstr_valid(cstr_t*);
void cstr_free(cstr_t*);
bool cstr_resize(cstr_t*,unsigned int);

void str_copy(void*, unsigned int, void*, unsigned int);


#endif
