#include "stdio.h"
#include "stdlib.h"

#include "cstr.h"

void print_cstr_cmp(cstr_t* a, cstr_t* b) {
  int result = cstr_cmp(a, b);
  cstr_print(a, "a");
  cstr_print(b, "b");
  printf("%s ?= %s :: %d\n", a->data, b->data, result);
}

int main(int argc, char **argv) {
  char test[] = "test string!\0";

  printf("%s :: %d\n", test, str_length(test));

  cstr_t* tmp = cstr("testing asdf cstr");
  cstr_print(tmp, NULL);
  cstr_del(tmp);

  cstr_t* a = cstr("String AAA.");
  cstr_t* b = cstr("String BBB.");
  cstr_print(a, "a");
  cstr_print(b, "b");

  tmp = cstr_concat(a, b);
  cstr_print(tmp, "tmp");

  cstr_concat_in(tmp, a);
  cstr_print(tmp, "tmp");

  cstr_concat_in(tmp, b);
  cstr_print(tmp, "tmp");

  cstr_del(tmp);

  char asdf[] = "::ASDF STRING ASDF::\0";
  tmp = cstr_strcat(a, asdf); 
  cstr_print(tmp, "tmp");

  cstr_strcat_in(tmp, asdf); 
  cstr_print(tmp, "tmp");
 
  cstr_strcat_in(tmp, asdf); 
  cstr_print(tmp, "tmp");

  cstr_concat_in(tmp, b);
  cstr_print(tmp, "tmp");

  cstr_del(tmp);

  cstr_del(a);
  cstr_del(b);



  a = cstr("aaa");
  b = cstr("aab");
  cstr_t* c = cstr("aaaa");

  print_cstr_cmp(a,a);
  print_cstr_cmp(a,b);
  print_cstr_cmp(b,a);
  print_cstr_cmp(b,b);
  print_cstr_cmp(c,c);
  print_cstr_cmp(a,c);
  print_cstr_cmp(c,a);
  print_cstr_cmp(c,b);
  print_cstr_cmp(b,c);

  cstr_del(a);
  cstr_del(b);
  cstr_del(c);





  return 0;
}

