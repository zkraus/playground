global _main
extern xsum


section .data

msg db 'Hello World!',0xa
len equ $ - msg

global _start
section .code
_start:
  mov edx, len
  mov ecx, msg
  mov ebx, 1
  mov eax, 4
  int 0x80

  mov eax, 6
  push eax
  mov eax, 5
  push eax
  call xsum
  sub esp, 8

  mov eax, 1
  mov ebx, 0
  int 0x80


