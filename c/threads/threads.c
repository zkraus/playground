#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>

#define THR_CNT 30
#define THR_LOOP_CNT 100


void
*worker(void* x) {
  char c = *(char*)x;
  for (int i=0; i < THR_LOOP_CNT; i++) {
    putchar(c);
  }
}

int
main() {
  char data_pool[] = {
   'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
   'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
   'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
   'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8',
   '9', '0'
  };

  char *p_data = data_pool;
  pthread_t pool[THR_CNT];
  pthread_t *p_pool = pool;
  size_t step = sizeof(pthread_t);
  size_t step_data = sizeof(char);

  for (int i=0; i < THR_CNT; i++, p_pool+=step, p_data+=step_data) {
    pthread_create(p_pool, NULL, worker, p_data);
  }
  p_pool = pool;
  for (int i=0; i < THR_CNT; i++, p_pool+=step) {
    pthread_join(*p_pool, NULL);
  }

  printf("\n");


  return 0;
}
