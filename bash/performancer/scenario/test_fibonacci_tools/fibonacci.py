#!/usr/bin/env python
import sys

def fib(N):
  L = [0]
  if N > 0:
    L.append(1)
  while len(L) <= int(N):
    L.append(L[-1] + L[-2])
  return L

def onefib(N):
  L = fib(N)
  return L[-1]

print onefib(sys.argv[1])
