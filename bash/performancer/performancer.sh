#!/usr/bin/env bash

_PERF_TEST_LIST=( $@ )
LOGLEVEL=debug
LOGDIR=log
LOGPREFIX=
MEASUREPREFIX=.measure
LOGFILE=log_performancer.log
OUTFILE=out_performancer.log
# 1: trace 2: debug 3: info 4: warning 5: error


function _log {
  echo "[$(date '+%T')] ${@}"
}
function _clog {
  if [[ $LOGLEVEL =~ $1 ]]; then
    shift
    _log "$@" | tee -a ${LOGDIR:+${LOGDIR}/}${LOGFILE}
  fi
}
function _trace {
  _clog "trace" "TRACE $@"
}
function _debug {
  _clog "trace|debug" "DEBUG $@"
}
function _info {
  _clog "trace|debug|info" "INFO $@"
}
function _warn {
  _clog "trace|debug|info|warning" "WARNING $@"
}
function _err {
  _clog "trace|debug|info|warning|error" "ERROR $@"
}

function _precondition_check {
  : #pass
}

function _load_test {
  if [[ -z "${1}" ]]; then
    echo "Error: no test was specified"
    exit 2
  fi

  source ${1}
  if (( $? != 0 )); then
    _err "failed to load perftest file"
    exit 2
  fi

  #PERFTEST_INPUT=${PERFTEST_INPUT:-( cnt )}
  PERFTEST_SCALABLE=${PERFTEST_SCALABLE:-false}
  PERFTEST_OUTPUTABLE=${PERFTEST_OUTPUTABLE:-false}
  PERFTEST_COMPARABLE=${PERFTEST_COMPARABLE:-false}

  if [[ -z "${PERFTEST_NAME}" ]]; then
    _err "perftest has no name, please specify into PERFTEST_NAME"
    exit 2
  fi

  _info "test \"${1}\" sucessfully loaded"
  LOGPREFIX="perftest_$(echo ${PERFTEST_NAME} | tr " " "_")"
  mkdir -p ${LOGDIR:+${LOGDIR}/}${LOGPREFIX} 2>/dev/null
  mkdir ${LOGDIR:+${LOGDIR}/}${MEASUREPREFIX} 2>/dev/null
}

function _make_header {
: #pass
  for var in ${!PERFTEST_*}; do
    _debug "\$${var}=${!var}"
  done

  echo "#=== PERFTEST =========================================#"
  echo "* NAME: ${PERFTEST_NAME}"
  echo -e "* DESC: ${PERFTEST_DESCRIPTION}"
  echo "* UNITS: ${PERFTEST_UNITS[@]}"
  echo "* INPUTS: ${PERFTEST_INPUT[@]}"
  local f=
  f+=" SCALE"; [[ ${PERFTEST_SCALABLE} == true ]] && f+=+ || f+=-
  f+=" OUT"; [[ ${PERFTEST_OUTPUTABLE} == true ]] && f+=+ || f+=-
  f+=" PRINT";[[ ${PERFTEST_PRINTABLE} == true ]] && f+=+ || f+=-
  f+=" COMP"; [[ ${PERFTEST_COMPARABLE} == true ]] && f+=+ || f+=-
  echo "* FLAGS: ${f}"
  echo "* DATETIME: $(date '+%F %T')"
  echo "* SYSTEM: $(uname -a)"
  echo "* BASH: $(bash --version | head -n 1)"
  echo
}

function _clean_environment {
  unset ${!PERFTEST_*}
  unset perftest_init perftest_compare
  unset _out_prefix _mes_prefix
}

function _make_footer {
  echo "#=== END ==============================================#"
}

function _run_case {
  (( _tcid++ ))
  local inputs=( )
  if (( "${#PERFTEST_INPUT[@]}" != 0)); then
    inputs=( $(echo ${PERFTEST_INPUT[@]} | sed 's/\([^ ]*\)/${\1}/g') )
  fi
  echo "+-- TESTCASE $(printf '%04d' ${_tcid} ) -------------------------------------+" 
  if [[ $PERFTEST_SCALABLE == true ]]; then
    echo "* SCALE: ${_scale}"
  fi
  echo "* CASE: ${1} "
  echo "* IN_VARS: ${inputs[@]}"
  echo "* IN_DATA: $(eval echo ${inputs[@]})"
  echo "* START: $(date '+%T.%N')"
  
  _out_prefix=${LOGDIR:+${LOGDIR}/}${LOGPREFIX:+${LOGPREFIX}/}
  _mes_prefix=${LOGDIR:+${LOGDIR}/}${MEASUREPREFIX:+${MEASUREPREFIX}/}
  local _measure_tm=${_mes_prefix}_measure_${PERFTEST_NAME}_${1}.tm
  local _measure_out=${_out_prefix}_output_${1}.txt

  local out=
  local prnt=
  if [[ ${PERFTEST_OUTPUTABLE} == true || ${PERFTEST_COMPARABLE} == true ]]; then
    out="${_measure_out}"
  else
    out="/dev/null"
  fi
  if [[ ${PERFTEST_PRINTABLE} == true ]]; then
    prnt="/proc/self/fd/1"
  else
    prnt="/dev/null"
  fi

  ## run in subshell
  ( 
    time ${@} $(eval echo ${inputs[@]})
  ) 2> ${_measure_tm} | tee ${out} 1> ${prnt}

  echo "* DURA: $(grep "real" ${_measure_tm} | tr -d "\t" | sed 's/real//')"
  echo
}

function _caseset_evaluate {
  echo "+-- EVALUATION -------------------------------------+" 
  if [[ $PERFTEST_COMPARABLE == true ]]; then
    if type "perftest_compare" &>/dev/null; then
      local ethalon=${_out_prefix}_output_perfcase_${PERFTEST_UNITS[0]}.txt
      echo "Compare ethalon: ${PERFTEST_UNITS[0]}"
      local perfcase=
      for perfcase in ${PERFTEST_UNITS[@]:1}; do
        case_out=${_out_prefix}_output_perfcase_${perfcase}.txt
        perftest_compare $ethalon $case_out &>/dev/null
        local result=$?
        echo -n "-> ${perfcase}"
        if (( $result == 0 )); then
          echo -e "\033[40G ...  \033[01;32mOK\033[00m  "
        else
          echo -e "\033[40G ... \033[01;31mFAIL\033[00m "
        fi
      done


    else
      _err "test init function not exist, please define it!"
    fi
  fi

}

function _run_caseset {
  for _case in ${PERFTEST_UNITS[@]}; do
      _run_case perfcase_${_case}
  done
  _caseset_evaluate
}

function _run_perftest {
  _load_test ${1}
  _make_header

  if type "perftest_init" &>/dev/null; then
  : #pass 
  else
    _err "test init function not exist, please define it!"
    exit 2
  fi


  for _scale in {1..5}; do
    if [[ $PERFTEST_SCALABLE == true ]]; then
      _info "SCALE CHANGE -> ${_scale}"
    fi
    perftest_init
    _run_caseset
    if [[ $PERFTEST_SCALABLE == false ]]; then
      break
    fi
  done

  _make_footer

}

_tcid=0

# ---------------------------------------------------------------------- #

for _perftest in ${_PERF_TEST_LIST[@]}; do
## prepare path for test cases
  export _perftest_path="$(dirname "$(readlink -f ${_perftest})")"
  _run_perftest ${_perftest}
  echo
  _clean_environment
done




