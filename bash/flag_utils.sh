#!/usr/bin/env bash

function flag_assign { #1: flag name 2: true|false
  local tmp=+
  if [[ ${2} == true || ${2} == "+" ]]; then
    tmp=+
      elif [[ ${2} == false || ${2} == "-" ]]; then
    tmp=-
  fi
  local prefix=$(echo $0.$$ | md5sum | tr -d " -")
  eval _FLAG_${prefix}_${1}=${tmp}
}

function flag_set { #1: flag name
  flag_assign $1 "true"
}

function flag_reset { #1: flag name
  flag_assign $1 "false"
}

function flag_get { #1: flag name
  local prefix=$(echo $0.$$ | md5sum | tr -d " -")
  local tmp=$(eval echo "\${_FLAG_${prefix}_${1}}")
  echo ${tmp:-'-'}
}

function flag_test { #1: flag name
  [[ $(flag_get $1) == "+" ]]
}

function flag_print {
  echo ${1}$(flag_get ${1})
}

function flag_print_all {
  local prefix=$(echo $0.$$ | md5sum | tr -d " -")
  local flags=( $(eval echo "\${!_FLAG_${prefix}_*}") )
  flags=( ${flags[@]//_FLAG_${prefix}_/} )
  local flag=
    for flag in ${flags[@]}; do
    echo -n "$(flag_print ${flag})"
    if [[ ${flag} != ${flags[@]:(-1)} ]]; then
      echo -n " "
      fi
      done
}

function flag_remove_all {
  local prefix=$(echo $0.$$ | md5sum | tr -d " -")
  local flags=( $(eval echo "\${!_FLAG_${prefix}_*}") )
  unset ${flags[@]}
}

# eof

