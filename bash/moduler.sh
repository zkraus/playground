#!/usr/bin/env bash

function _load_module { #1: path to module
  local result=0
  local filename="$1.sh"
  if [[ -f $filename && -r $filename ]]; then
    if [[ $(file $filename) =~ "bash script" ]]; then
      source "$filename"
      ecode=$?
      if [[ $ecode == 0 ]]; then
        : #pass
      else
        result=1
      fi
    else
      result=1
    fi
  else
    result=1
  fi
   
  local result_text=""
  if [[ $result == 0 ]]; then
    result_text="[  OK  ]"
  else
    result_text="[ FAIL ]"
  fi  
  echo " :: ${result_text} ... load module $(basename $1)"
  return $result
}

function _load_module_list { #1: list of modules
  local result=0
  for mod in $@; do
    _load_module ${mod}
    (( result += $? ))
  done

  local result_text=""
  if [[ $result == 0 ]]; then
    result_text="[ DONE ]"
  else
    result_text="[ FAIL ]"
  fi  
  echo " == ${result_text} === loading list of modules"
  return $result
  
}
