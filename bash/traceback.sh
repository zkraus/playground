#!/usr/bin/env bash

function exit {
  traceback
  unset exit
  exit $1
}

function traceback { 
	local nesting_correction=2
	[[ $0 =~ "bash" ]] && nesting_correction=1
	echo " >> Traceback [$(date '+%T %F')]"
	for ((i=$(( ${#FUNCNAME[@]} - nesting_correction )); i>0; --i)); do 
	  echo " ${i}: ${FUNCNAME[i]} @ ${BASH_SOURCE[i+1]:-.}[${BASH_LINENO[i]}]"
	done
	echo " .: ${FUNCNAME[0]} @ ${BASH_SOURCE[1]:-.}[${BASH_LINENO[0]}]"
}

