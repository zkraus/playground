# Zkraus' Playground


## Contents:
separate playgrounds for:

* bash
* C
* C++
* python
* cmake
* java

Within these playground, there are various examples, and tryouts of code, techniques, etc.
For example: pointers, lists, annotations, iterators,...

## Authors:
* Zdenek Kraus <zdenek.kraus@gmail.com>

## References:
* *c/gif2bmp*: code is part of (my) student project and image data are owned by Faculty of Information Technology, Brno University of Technology (http://www.fit.vutbr.cz/)
* *cmake/tutorial*: was created based on official CMake tutorial (https://cmake.org/cmake-tutorial/) might not be precise

## default licenses
for others non specified (if applicable):

* **LGPL 3.0** for code items
* **CC-BY-SA-3.0** for non-code art items (if applicable)


|asdf|asdf|
|----|----|
|qwer|qwer|
